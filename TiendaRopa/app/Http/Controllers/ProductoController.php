<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
    public function inicioAd()
    {
        return view("inicioAd");
    }

    public function AgregarProducto()
    {
        $Prod = Producto::get();

        return view("ProductoNuevo",compact('Prod'));
    }
    public function menu()
    {
        $Prod = Producto::get();

        return view("menu",compact('Prod'));
    }

    public function crear()
    {
        $producto = new Producto();
        $producto->Nombre = "asd";
        $producto->correo = "asd";
        $producto->password = "asd";
        $verificar = $producto->save();
        if ($verificar) {
            echo json_encode(["estatus" => "success"]);
        } else {
            echo json_encode(["estatus" => "error"]);
        }
    }

    public function productosForm(Request $productos)
    {

       if (!$productos->nombre || !$productos->marca || !$productos->descripcion || !$productos->existencia || !$productos->precio) {

          return view("ProductoNuevo",compact('Prod'), ["estatus" => "error", "mensaje" => "¡Falta información!"]);

        } else {
            $productos->validate([
                'imagen' => 'required|image|max:2048'
            ]);
            $productos->validate([
                'imagen2' => 'required|image|max:2048'
            ]);


            $imagenes1 = $productos->file('imagen')->store('public/imagenes');
            $imagenes2 = $productos->file('imagen2')->store('public/imagenes');
            $url1 = Storage::url($imagenes1);
            $url2 = Storage::url($imagenes2);




            $producto = new Producto();
            $producto->nombre = $productos->nombre;
            $producto->marca= $productos->marca;
            $producto->imagen = $url1;
            $producto->imagen2 = $url2;
            $producto->descripcion= $productos->descripcion;
            $producto->existencia = $productos->existencia;
            $producto->precio = $productos->precio;
          //  echo json_encode($producto);
         $producto->save();
           $Prod = Producto::get();
            return view("ProductoNuevo", ["estatus" => "success", "mensaje" => "¡Producto Creado!"]);
        }
    }
    public function ProductoList(){
        $Prod = Producto::all();
        return view("Productos",compact('Prod'));

    }

    public function all(){
        $Prod = Producto::all();
        return response(json_encode($Prod),200)->header('Content-type','text/plain');
    }


}



