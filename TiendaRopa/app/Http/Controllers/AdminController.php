<?php

namespace App\Http\Controllers;

use App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use validator;

class AdminController extends Controller
{
    public function mostrarTodo()
    {
        $admin = Admin::get();
        if($admin){
            echo json_encode(["estatus" => "success","usuarios" => $admin]);
        }else{
            echo json_encode(["estatus" => "error"]);
        }
    }

    public function mostrar($id)
    {
        $admin = Admin::find($id);
        if($admin){
            echo json_encode(["estatus" => "success","usuario" => $admin]);
        }else{
            echo json_encode(["estatus" => "error"]);
        }
    }
    public function crear()
    {
        $admin = new Admin();
        $admin->Nombre = "";
        $admin->correo = "";
        $admin->password = "";
        $verificar = $admin->save();
        if($verificar){
            echo json_encode(["estatus" => "success"]);
        }else{
            echo json_encode(["estatus" => "error"]);
        }
    }

    public function AgregarAdmin(){

        return view("AgregarAdmin");
    }
    public function AdministradoresList(){
        $Admin = Admin::all();
        return view("Administradores",compact('Admin'));
    }

    public function registroForm(Request $datos){

        if(!$datos->nombre ||!$datos->correo || !$datos->password1 || !$datos->password2)

            return view("AgregarAdmin",["estatus"=> "error", "mensaje"=> "¡Falta información!"]);

        $admin = Admin::where('correo',$datos->correo)->first();
        if($admin)

            return view("AgregarAdmin", ["estatus"=> "error", "mensaje"=> "¡El correo ya se encuentra registrado!"]);
        $nombre = $datos->nombre;
        $correo = $datos->correo;
        $password2 = $datos->password2;
        $password1 = $datos->password1;

        if($password1 != $password2){

            return view("AgregarAdmin",["estatus" => "¡Las contraseñas son diferentes!"]);
        }

        $admin = new Admin();
        $admin->nombre = $nombre;
        $admin->correo = $correo;
        $admin->password = bcrypt($password1);
        $admin->save();

        return view("AgregarAdmin",["estatus" => "Agregado correctamente"]);

       // echo json_encode($admin);
    }


    public function all(){
        $Admin = Admin::all();
       return response(json_encode($Admin),200)->header('Content-type','text/plain');
    }

    public function verificarCredenciales(Request $datos){

        if(!$datos->correo || !$datos->password)
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);

        $admin = Admin::where("correo",$datos->correo)->first();
        if(!$admin)
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);

        if(!Hash::check($datos->password,$admin->password))
            return view("loginAdmin",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

        Session::put('administrador',$admin);

        if(isset($datos->url)){
            $url = decrypt($datos->url);
            return redirect($url);
        }else{
            return redirect()->route('admin.inicioAd');
        }

    }

    public function cerrarSesion(){
        if(Session::has('administrador'))
            Session::forget('administrador');

        return redirect()->route('bienvenida');
    }
    public function ad(){
        return view("loginAdmin");
    }




}
