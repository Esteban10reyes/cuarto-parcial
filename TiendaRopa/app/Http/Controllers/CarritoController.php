<?php

namespace App\Http\Controllers;

use App\Models\carrito;
use App\Models\Producto;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class CarritoController extends Controller
{


    public function carritoV()
    {
      return view('carrito');
    }

    public function All(){
        $Prod = carrito::all();
        return response(json_encode($Prod),200)->header('Content-type','text/plain');
    }

    public function carritoForm(Request $data){
      $array=json_decode($data->arrayList);
        $val=array();

        foreach ($array as $uso){
            $val[] = $uso;
        }


        $otro=array_count_values($val);



        foreach ($otro as $car=> $value){

            $Carritos = new carrito();
            //$precio = Producto::where('id_prod',$car->id_prod)->firs();
            $precio = Producto::find($car->id_prod);
            $Carritos -> id_Usuario= session('usuario')->id;
            $Carritos -> id_producto= $precio;
            $Carritos -> producto= $precio->nombre;
            $Carritos-> marca= $precio->marca;
            $Carritos-> cantidad= $value;
            $Carritos-> descripcion= $precio->descripcion;
            $total=$precio->precio*$value;
            $Carritos -> total=$total;

            $Carritos -> save();


        }

    }
}

