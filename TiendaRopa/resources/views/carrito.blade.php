@extends('layout.main')

@section('titulo')
    <title>Carrito | Spotr Stellar</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')




@endsection

@section('contenido')
    <div class="row">
        @csrf
        <div class="table-responsive">
            <table class="table">
                <caption>Carrito de compras</caption>
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">PRODUCTO</th>
                    <th scope="col">MARCA</th>
                    <th scope="col">CANTIDAD</th>
                    <th scope="col">DESCRIPCION</th>
                    <th scope="col">TOTAL</th>

                </tr>
                </thead>
                <tbody id="tbody">




                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){

            $.ajax({
                url:'/usuario/menu/lista',
                method:'POST',
                data:{
                    id:1,
                    _token:$('input[name="_token"]').val()
                },

            }).done(function(res){
                var arreglo =JSON.parse(res);
                for(var x=0;x < arreglo.length;x++){

                    var todo='<tr><td>'+ arreglo[x].id_carrito+'</td>';
                    todo+='<td>'+arreglo[x].producto+'</td>';
                    todo+='<td>'+arreglo[x].marca+'</td>';
                    todo+='<td>'+arreglo[x].cantidad+'</td>';
                    todo+='<td>'+arreglo[x].descripcion+'</td>';
                    todo+='<td>'+arreglo[x].total+'</td>';
                    todo+='</tr>';
                    console.log(todo);

                    $('#tbody').append(todo);

                }
            });

        });

    </script>

@endsection

