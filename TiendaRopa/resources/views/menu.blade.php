@extends('layout.main')

@section('titulo')
    <title>Inicio | Spotr Stellar</title>
@endsection


@section('css')

@endsection

@section('notificacion')
    <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">

            <!-- Contador de productos -->

            <form  method="get" action="{{route('carritoV')}}">
                <span id="cantidadCarrito" class="badge badge-danger badge-counter"></span>
                <button id="subir" type="submit" href="{{route('carritoV')}}" class="fas fa-shopping-cart "> </button>
            </form>

        </a>
        <!-- Dropdown - Messages -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
             aria-labelledby="messagesDropdown">

            <!-- Producto 1 -->
            <!-- fin de producto -->


        </div>
    </li>
@endsection

@section('titulo-pagina')

@endsection

@section('contenido')


    <div class="row">
    @foreach($Prod as $ad)
        <!-- Earnings (Monthly) Card Example -->

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="h1 mb-0 font-weight-bold text-gray-800">{{$ad->nombre}}</div>
                                <div><img src="/public/{{asset($ad->imagen)}}" class="img-thumbnail img-fluid"></div>
                                <div class="text-x font-weight-bold text-primary text-uppercase mb-1">Precio:
                                    ${{$ad->precio}}</div>
                                <div class="my-2"></div>


                                <button id="contar{{$loop->index+1}}" class="btn btn-success btn-icon-split">Agregar al
                                    carrito
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>




@endsection

@section('js')
    <script>

        let datosArray = [];
        $(document).ready(function () {

            let numero = 0;
            @foreach($Prod as $uso)
            $("#contar{{$loop->index+1}}").click(function (e) {
                e.preventDefault();
                numero++;

                $("#cantidadCarrito").html(numero);
                datosArray[numero - 1] = {{$uso->id_prod}}


            });
            @endforeach
            peticionAjax();

            function peticionAjax() {
                $("#subir").click(function(){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "post",
                        url: "{{route('carrito.subir')}}",
                        data:{'arrayList':JSON.stringify(datosArray)},
                        success: function (data) {
                            console.log(data);
                            console.log("se subio bien");
                        }
                    });
                });
            }
        });

    </script>



@endsection

