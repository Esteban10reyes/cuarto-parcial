@extends('layout.admin-main')

@section('titulo')
    <title>Productos | Spotr Stellar</title>
@endsection

@extends('layout.admin-main')

@section('titulo')
    <title>Ventas | Spotr Stellar</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bienvenido . Lista de ventas</h1>

    </div>
    <div class="table-responsive">
        <table class="table">
            <caption>Lista de Productos</caption>
            <thead>
            <tr>

                <th scope="col">Nombre</th>
                <th scope="col">Marca</th>
                <th scope="col">Descripcion</th>
                <th scope="col">total</th>



            </tr>
            </thead>
            <tbody>
            @foreach($venta as $ad)
                <tr>


                    <td>{{$ad->producto}}</td>
                    <td>{{$ad->marca}}</td>
                    <td>{{$ad->descripcion}}</td>
                    <td>${{$ad->total}}</td>


                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


@endsection

@section('contenido')

@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTable').DataTable();
        });

    </script>

@endsection
