<!DOCTYPE HTML>
<!--
	Stellar by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Sport Stellar</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="/public/principal/assets/css/main.css" />
    <noscript><link rel="stylesheet" href="/public/principal/assets/css/noscript.css" /></noscript>
</head>
<body class="is-preload">

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header -->
    <header id="header" class="alt">
        <span class="logo"><img src="/public/principal/images/logo.png" alt="" /></span>
        <h1>Sport Stellar</h1>
        <p>El mundo del deporte, el mundo de la felicidad<br />
            Futbol * Innovacion * Uniforme * Accesorios * calzado *  <a href="#">Diversion</a>.</p>
    </header>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="#inicio" class="active">La mejor Tienda</a></li>
            <li><a href="#Novedades">Novedades</a></li>
            <li><a href="#Inicia">Inicia Sesion</a></li>
        </ul>
    </nav>

    <!-- Main -->
    <div id="main">

        <!-- Introduction -->
        <section id="inicio" class="main">
            <div class="spotlight">
                <div class="content">
                    <header class="major">
                        <h2>La mejor tienda</h2>
                    </header>
                    <p>Aqui encontraras los mejores precios en el mejor lugar llena tu carrito y asi podras disfrutar de las mejores ofertas Entra y disfruta.</p>
                    <ul class="actions">
                        <li><a href="{{route('log')}}" class="button">Compra Ya!!</a></li>
                    </ul>
                </div>
                <span class="image"><img src="/public/principal/images/rop.jpg" alt="" /></span>
            </div>
        </section>

        <!-- First Section -->
        <section id="Novedades" class="main special">
            <header class="major">
                <h2>Novedades...</h2>
            </header>
            <ul class="features">
                <li>
                    <span class="icon solid major style1 fa-shopping-bag"></span>
                    <h3>Tu Tienda en linea</h3>
                    <p>La mejores marcas y diseños en este lugar no esperes mas.</p>
                </li>
                <li>
                    <span class="icon major style3 fa-credit-card"></span>
                    <h3>Los mejores precios</h3>
                    <p>Como nunca antes visto los precios mas bajos del mercado con la mejor calidad</p>
                </li>
                <li>
                    <span class="icon major style5 fa-gem"></span>
                    <h3>Innovasion</h3>
                    <p>Lamejor ropa y accesorios del mercado, la mejor opcion para tus eventos deportivos.</p>
                </li>
            </ul>

        </section>


        <!-- Get Started -->
        <section id="Inicia" class="main special">
            <header class="major">
                <h2>Iniciar sesion</h2>
                <p>Inicia sesion para poder comprar en nuestra tienda online<br />
                    Lo mejor en Marcas y Precios.</p>
            </header>
            <footer class="major">
                <ul class="actions special">
                    <li><a href="{{route('log')}}" class="button primary" >Iniciar Sesion</a></li>
                </ul>
            </footer>
        </section>

    </div>

    <!-- Footer -->
    <footer id="footer">
        <section>
            <h2>Registrate</h2>
            <p>Registrate para poder comprar</p>
            <ul class="actions">
                <li><a href="{{route('registro')}}" class="button" href="#">Registrate</a></li>
            </ul>
        </section>
        <section>
            <h2>Informacion</h2>
            <dl class="alt">
                <dt>Ubicacion</dt>
                <dd>Ecatepec Estado de Mexico &bull; Cerro Laboratorio &bull; CP.55067</dd>
                <dt>Whatsapp</dt>
                <dd>5610554113</dd>
                <dt>Email</dt>
                <dd><a href="#">estebanreyes1390@gmail.com</a></dd>
            </dl>
            <ul class="icons">
                <li><a href="https://twitter.com/Esteban_reyes12?s=09" class="icon brands fa-twitter alt"><span class="label">Twitter</span></a></li>
                <li><a href="https://www.facebook.com/esteban10reyes10" class="icon brands fa-facebook-f alt"><span class="label">Facebook</span></a></li>
                <li><a href="https://www.instagram.com/p/CF0asE9ABIexkjQrAJoqcn026k3UCM-IfVwVu80/?igshid=yeo77q13tem9" class="icon brands fa-instagram alt"><span class="label">Instagram</span></a></li>
                <li><a href="{{route('ad')}}" class="icon brands fa-umbrella  alt"><span class="label">Admin</span></a></li>
            </ul>
        </section>
        <p class="copyright">&copy; Diseñado y creado: <a href="https://www.facebook.com/esteban10reyes10">Reyes Anguiano Esteban</a>.</p>
    </footer>

</div>

<!-- Scripts -->
<script src="/public/principal/assets/js/jquery.min.js"></script>
<script src="/public/principal/assets/js/jquery.scrollex.min.js"></script>
<script src="/public/principal/assets/js/jquery.scrolly.min.js"></script>
<script src="/public/principal/assets/js/browser.min.js"></script>
<script src="/public/principal/assets/js/breakpoints.min.js"></script>
<script src="/public/principal/assets/js/util.js"></script>
<script src="/public/principal/assets/js/main.js"></script>

</body>
</html>
