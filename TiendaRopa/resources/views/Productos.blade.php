@extends('layout.admin-main')

@section('titulo')
    <title>Productos | Spotr Stellar</title>
@endsection

@section('css')

@endsection


@section('titulo-pagina')


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bienvenido . Tu Productos Agregados</h1>

    </div>
    <div class="table-responsive">
        <table class="table">
            <caption>Lista de Productos</caption>
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>

                <th scope="col">Marca</th>
                <th scope="col">Imagen 1</th>
                <th scope="col">Imagen 2</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Existencia</th>
                <th scope="col">Precio</th>


            </tr>
            </thead>
            <tbody>
            @foreach($Prod as $ad)
                <tr>

                    <td>{{$ad->id_prod}}</td>
                    <td>{{$ad->nombre}}</td>
                    <td>{{$ad->marca}}</td>
                    <td><img src="{{asset($ad->imagen)}}" class="img-thumbnail img-fluid" width="50" length="-50"></td>
                    <td><img src="{{asset($ad->imagen2)}}"class="img-thumbnail img-fluid" width="50" length="-50"></td>
                    <td>{{$ad->descripcion}}</td>
                    <td>{{$ad->existencia}}</td>
                    <td>{{$ad->precio}}</td>

                </tr>
            @endforeach

            </tbody>
        </table>
    </div>


@endsection

@section('contenido')

@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){
            $('#dataTable').DataTable();
        });

    </script>

@endsection

