@extends('layout.admin-main')

@section('titulo')
    <title>Productos | Spotr Stellar</title>
@endsection

@extends('layout.admin-main')

@section('titulo')
    <title>Agregar Administrador | Spotr Stellar</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bienvenido . Agrega Nuevos Productos</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal"
           data-target="#modalAgregar"><i
                class="fas fa-download fa-sm text-white-50"></i> Agregar Producto</a>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="modalAgregar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Producto</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="user" action="{{route('AgregarProducto.form')}}" method="post">
                      enctype="multipart/form-data" accept="image/*">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nombre Producto"
                                   id="exampleInputName" aria-describedby="nombre" name="nombre" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Marca"
                                   id="exampleInputName" aria-describedby="marca" name="marca" required/>
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control" placeholder="Imagen "
                                   id="exampleInputName" aria-describedby="imagen" name="imagen" required/>
                            <br>
                            @error('imagen')
                            <small class="text-danger">{{$message}} </small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="file" class="form-control" placeholder="Imagen2"
                                   id="exampleInputName" aria-describedby="imagen2" name="imagen2" required/>
                            <br>
                            @error('imagen2')
                            <small class="text-danger">{{$message}} </small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="descripcion"
                                   id="exampleInputName" aria-describedby="descripcion" name="descripcion" required/>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" placeholder="Existencia"
                                   id="exampleInputName" aria-describedby="existencia" name="existencia" required/>
                        </div>

                        <div class="form-group">
                            <input type="number" class="form-control" placeholder="Precio"
                                   id="exampleInputName" aria-describedby="precio" name="precio" required/>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('contenido')

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            @if($message=Session::get('ErrorInser'))
            $("#modaAgregar").modal('show');
            @endif
        });
    </script>
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable();
        });
@endsection

