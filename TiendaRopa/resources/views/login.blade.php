<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes"><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans'><link rel="stylesheet" href="/public/login/style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="cont">
    <div class="demo">
        <div class="login">
            <div class="login__check"></div>
            @if(isset($estatus))
                @if($estatus == "success")
                    <label class="text-success">{{$mensaje}}</label>
                @elseif($estatus == "error")
                    <label class="text-warning">{{$mensaje}}</label>
                @endif
            @endif
            <form class="user" action="{{route('log.form')}}" method="post">
                {{csrf_field()}}
            <div class="login__form">
                <div class="login__row">
                    <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                        <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                    </svg>
                    <input type="email" class="login__input name" placeholder="Correo"
id="exampleInputEmail" aria-describedby="emailHelp"  name="correo" required/>
                </div>
                <div class="login__row">
                    <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                        <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
                    </svg>
                    <input type="password" class="login__input pass" placeholder="Contraseña"
                           id="exampleInputPassword" placeholder="Password" name="password" required/>
                </div>

                <button type="submit" class="login__submit">Iniciar sesion</button>
                <hr>
                @if(isset($_GET["r"]))
                    <input type="hidden" name="url" value="{{$_GET["r"]}}">
                @endif
            </form>
            <hr>
                <p class="login__signup">Ya tienes cuenta? &nbsp;<a href="{{route('registro')}}">Registrate</a></p>
            </div>
        </div>

    </div>
</div>
<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script  src="./script.js"></script>

</body>
</html>
