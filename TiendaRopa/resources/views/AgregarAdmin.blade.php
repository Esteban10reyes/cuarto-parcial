@extends('layout.admin-main')

@section('titulo')
    <title>Agregar Administrador | Spotr Stellar</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')


    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bienvenido . Agrega Nuevos Administrador</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal"
           data-target="#modalAgregar"><i
                class="fas fa-download fa-sm text-white-50"></i> Agregar Administrador</a>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAgregar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Admin</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="user" action="{{route('adminRegistro.form')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Nombre "
                                   id="exampleInputName" aria-describedby="nombre" name="nombre" required/>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Correo"
                                   id="exampleInputName" aria-describedby="correo" name="correo" required/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="password"
                                   id="exampleInputName" aria-describedby="pasword" name="password1" required/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="password"
                                   id="exampleInputName" aria-describedby="Repetir pasword" name="password2" required/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('contenido')
<div class="row">
    <div class="table-responsive">
        <table class="table">
            <caption>Lista de Administradores</caption>
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Correo</th>
                <th scope="col">Creado</th>

            </tr>
            </thead>
            <tbody id="tbody">




            </tbody>
        </table>
    </div>
</div>
@endsection

@section('js')
    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function (){

            $.ajax({
                url:'/usuario/NuevoAdmin/all',
                method:'POST',
                data:{
                    id:1,
                    _token:$('input[name="_token"]').val()
                },

            }).done(function(res){
                var arreglo =JSON.parse(res);
                for(var x=0;x < arreglo.length;x++){

                    var todo='<tr><td>'+ arreglo[x].id_Admin+'</td>';
                    todo+='<td>'+arreglo[x].nombre+'</td>';
                    todo+='<td>'+arreglo[x].correo+'</td>';
                    todo+='<td>'+arreglo[x].created_at+'</td>';
                    todo+='</tr>';
                    console.log(todo);

                    $('#tbody').append(todo);

                }
            });

        });

    </script>

@endsection

