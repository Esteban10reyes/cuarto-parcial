<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\ComprasController;
use App\Http\Controllers\CarritoController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('bienvenida');
});

    Route::get('/bienvenida',[UsuarioController::class,'bienvenida'])->name('bienvenida');
    Route::get('/log',[UsuarioController::class,'log'])->name('log');
    Route::post('/log',[UsuarioController::class,'verificarCredenciales'])->name('log.form');
    Route::get('/cerrarSesion',[UsuarioController::class,'cerrarSesion'])->name('cerrar.sesion');
    Route::get('/cerrarSesion/admin',[AdminController::class,'cerrarSesion'])->name('cerrar.sesion');
    Route::get('/registro',[UsuarioController::class,'registro'])->name('registro');
    Route::post('/registro',[UsuarioController::class,'registroForm'])->name('registro.form');
    Route::get('ad/',[AdminController::class,'ad'])->name('ad');
    Route::post('/ad',[AdminController::class,'verificarCredenciales'])->name('ad.form');



Route::prefix('/usuario')->middleware("VerificarUsuario")->group(function (){
    Route::get('/menu',[ProductoController::class,'menu'])->name('usuario.menu');
    Route::post('/menu/lista',[CarritoController::class,'all'])->name('meni.item');
    Route::post('/carrito/subir',[CarritoController::class,'carritoForm'])->name('carrito.subir');

    Route::get('/carrito',[CarritoController::class,'carritoV'])->name('carritoV');
    Route::post('/carrito',[CarritoController::class,'All'])->name('carrito.lista');

    // Route::post('/menu/all',[AdminController::class,'all'])->name('admin.AgregarAdmin.all');
    Route::get('/menu/ad',[ProductoController::class,'inicioAd'])->name('admin.inicioAd');
    Route::get('/NuevoProducto',[ProductoController::class,'AgregarProducto'])->name('admin.AgregarProducto');
    Route::post('/NuevoProducto',[ProductoController::class,'productosForm'])->name('AgregarProducto.form');
    Route::get('/NuevoAdmin',[AdminController::class,'AgregarAdmin'])->name('admin.AgregarAdmin');
    Route::post('/NuevoAdmin',[AdminController::class,'registroForm'])->name('adminRegistro.form');
    Route::post('/NuevoAdmin/all',[AdminController::class,'all'])->name('admin.AgregarAdmin.all');
    Route::get('/AdministradoresList',[AdminController::class,'AdministradoresList'])->name('Admin.List');
    Route::get('/ProductoList',[ProductoController::class,'ProductoList'])->name('Producto.List');
    Route::get('/Compras',[ComprasController::class,'Compra'])->name('Compras.lis');
    Route::get('/Mis/Compras',[ComprasController::class,'MisCompras'])->name('MisCompra.lis');

});



/*

Route::prefix('/admin')->middleware("VerificarAdmin")->group(function (){
    Route::get('/menu',[ProductoController::class,'inicioAd'])->name('admin.inicioAd');
    Route::get('/NuevoProducto',[ProductoController::class,'AgregarProducto'])->name('admin.AgregarProducto');
    Route::post('/NuevoProducto',[ProductoController::class,'productosForm'])->name('AgregarProducto.form');
    Route::get('/NuevoAdmin',[AdminController::class,'AgregarAdmin'])->name('admin.AgregarAdmin');
    Route::post('/NuevoAdmin',[AdminController::class,'registroForm'])->name('adminRegistro.form');
    Route::post('/NuevoAdmin/all',[AdminController::class,'all'])->name('admin.AgregarAdmin.all');
    Route::get('/AdministradoresList',[AdminController::class,'AdministradoresList'])->name('Admin.List');


});
*/
