<?php

namespace App\Http\Controllers;

use App\Models\Resultados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\correos;
class ResultadosController extends Controller
{
    public function subir(Request $datos)
    {
     $puntos=0;
     $correcto="1";
     if($datos->preguntaUno==$correcto){
         $puntos=$puntos+1;
     }
        if($datos->preguntaDos==$correcto){
            $puntos=$puntos+1;
        }
        if($datos->preguntaTres==$correcto){
            $puntos=$puntos+1;
        }
        if($datos->preguntaCuatro==$correcto){
            $puntos=$puntos+1;
        }

        $edad = $datos->edad;
        $genero = $datos->genero;

        $resultado=new Resultados();
        $resultado->id_usuario=session('usuario')->Id;
        $resultado->edad=$edad;
        $resultado->genero=$genero;
        $resultado->resultados=$puntos;
        $resultado->save();

        //enviar correo
/*
        $detalles=[

            'title'=>'Resultados de examen',
            'id'=>'ID:'.session('usuario')->id,
           'nombre'=>'Nombre:'.session('usuario')->nombre,
            'puntos'=>'Tu puntaje es'.$puntos

        ];
        Mail::to("estebanreyes1390@hotmail.com")->send(new correos($detalles));

        echo "Tus Puntos son $puntos";
             */
    }
    function MisResultados(){
        $resultados = Resultados::all();     //where("id_usuario",session('usuario')->id)->first();
        return view("resultados",compact('resultados'));
    }
    function todosResultados(){
        $resultados = Resultados::where("id_usuario",session('usuario')->id)->first();
        return view("resultados",['resultados'=>$resultados]);
    }
}
