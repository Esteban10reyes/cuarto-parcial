<?php

namespace App\Http\Controllers;

use App\Models\Resultados;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{
    //vistas
    //inicio
    public function bienvenida()
    {
        return view("bienvenida");
    }

//loagin
    public function login()
    {
        return view("login");
    }

//registros
    public function registro()
    {
        return view("registro");
    }

//pagina principal usuario
    public function principalU()
    {
        return view("usuarioPrin");
    }


    //loagin usuarios
    public function verificarCredenciales(Request $datos)
    {

        if (!$datos->correo || !$datos->password)
            return view("login", ["estatus" => "error", "mensaje" => "¡Completa los campos!"]);

        $usuario = Usuario::where("correo", $datos->correo)->first();
        if (!$usuario)
            return view("login", ["estatus" => "error", "mensaje" => "¡El correo no esta registrado!"]);

        if (!Hash::check($datos->password, $usuario->password))
            return view("login", ["estatus" => "error", "mensaje" => "¡Datos incorrectos!"]);

        Session::put('usuario', $usuario);

        if (isset($datos->url)) {
            $url = decrypt($datos->url);
            return redirect($url);
        } else {
            return redirect()->route('Usuario.principal');
        }

    }

//registrar Usuarios
    public function registroForm(Request $datos)
    {

        if (!$datos->correo || !$datos->password1 || !$datos->password2)
            return view("registro", ["estatus" => "error", "mensaje" => "¡Falta información!"]);

        $usuario = Usuario::where('correo', $datos->correo)->first();
        if ($usuario)
            return view("registro", ["estatus" => "error", "mensaje" => "¡El correo ya se encuentra registrado!"]);

        $nombre = $datos->nombre;
        $correo = $datos->correo;
        $password2 = $datos->password2;
        $password1 = $datos->password1;

        if ($password1 != $password2) {
            return view("registro", ["estatus" => "¡Las contraseñas son diferentes!"]);
        }

        $usuario = new Usuario();
        $usuario->nombre = $nombre;
        $usuario->correo = $correo;
        $usuario->password = bcrypt($password1);
        $usuario->save();
        return view("login", ["estatus" => "success", "mensaje" => "¡Cuenta Creada!"]);

    }
    //cerrar sesion
    public function cerrarSesion(){
        if(Session::has('usuario'))
            Session::forget('usuario');

        return view("bienvenida");
    }





}
