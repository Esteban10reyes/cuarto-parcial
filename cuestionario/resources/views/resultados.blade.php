@extends('layout.main')

@section('titulo')
    <title>Test | Test</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Bienvenido {{session('usuario')->nombre}}. Resultados de tu Test</h1>
@endsection

@section('contenido')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Bienvenido . Mis compras</h1>

    </div>
    <div class="table-responsive">
        <table class="table">
            <caption>Mis resultados</caption>
            <thead>
            <tr>

                <th scope="col">Nombre</th>
                <th scope="col">Puntaje</th>
                <th scope="col">fecha</th>




            </tr>
            </thead>
            <tbody>
            @foreach($resultados as $ad)
                <tr>


                    <td>{{session('usuario')->nombre}}</td>
                    <td>{{$ad->resultados}}</td>
                    <td>{{$ad->created_ad}}</td>



                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection

@section('js')

@endsection

