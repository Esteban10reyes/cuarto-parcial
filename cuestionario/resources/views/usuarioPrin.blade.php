@extends('layout.main')

@section('titulo')
    <title>Test | Test</title>
@endsection

@section('css')

@endsection

@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Bienvenido {{session('usuario')->nombre}}. Resuelve el cuestionario</h1>
@endsection

@section('contenido')
<form  method="post" action="{{route('subir.resultados')}}">

    {{csrf_field()}}

        <input type="text" class="form-control form-control-user"
               id="exampleInputEmail" aria-describedby="emailHelp"
               placeholder="Escribe tu edad" name="edad" required>

    <h4>Genero</h4>
    <input type="radio" name="genero" value="M">Masculino<br>
    <input type="radio" name="genero" value="F">Femenino<br>
    <h4>1.- ¿Cual es el lugar mas frio de la tierra?</h4>
    <input type="radio" name="preguntaUno" value="0">Russia<br>
    <input type="radio" name="preguntaUno" value="0">Su corazon<br>
    <input type="radio" name="preguntaUno" value="1">La antartida<br>

    <h4>2.- ¿Quien escribio la odisea?</h4>
    <input type="radio" name="preguntaDos" value="0">Ozuna<br>
    <input type="radio" name="preguntaDos" value="1">Homero<br>
    <input type="radio" name="preguntaDos" value="0">Platon<br>


    <h4>3.- ¿Cual es el rio mas largo del mundo?</h4>
    <input type="radio" name="preguntaTres" value="0">Nilo<br>
    <input type="radio" name="preguntaTres" value="0">Chapultepec<br>
    <input type="radio" name="preguntaTres" value="1">Amazonas<br>

    <h4>4.- ¿Como se llama la reina de Reino Unido?</h4>
    <input type="radio" name="preguntaCuatro" value="0">Perlita<br>
    <input type="radio" name="preguntaCuatro" value="1">Isabel<br>
    <input type="radio" name="preguntaCuatro" value="0">Fiona<br>
    <h4>5.- ¿En que continente esta ecuador?</h4>
    <input type="radio" name="preguntaCinco" value="1">America<br>
    <input type="radio" name="preguntaCinco" value="0">Africa<br>
    <input type="radio" name="preguntaCinco" value="0">Europa<br>

    <h4>6.- ¿Que cantidad de huesos tiene el cuerpo humano?</h4>
    <input type="radio" name="preguntaSeid" value="0">300<br>
    <input type="radio" name="preguntaSeid" value="0">78<br>
    <input type="radio" name="preguntaSeid" value="1">206<br>

    <h4>7.- ¿Cuando acabo la 2° Gerra Mundiar?</h4>
    <input type="radio" name="preguntaSiete" value="0">2020<br>
    <input type="radio" name="preguntaSiete" value="1">1945<br>
    <input type="radio" name="preguntaSiete" value="0">1990<br>
    <h4>8.- ¿Quien es el autor del quijote?</h4>
    <input type="radio" name="preguntaOcho" value="0">Sancho panza<br>
    <input type="radio" name="preguntaOcho" value="1">Miguel Cervantes<br>
    <input type="radio" name="preguntaOcho" value="0">Leonardo da Vinci<br>

    <h4>9.- ¿En donde se encuentra la torre de piaza?</h4>
    <input type="radio" name="preguntaNueve" value="0">Honk Kong<br>
    <input type="radio" name="preguntaNueve" value="1">Italia<br>
    <input type="radio" name="preguntaNueve" value="0">Mexico<br>

    <h4>10.- ¿Que deporte Practicaba Michael Jordan?</h4>
    <input type="radio" name="preguntaDiez" value="0">Futbol<br>
    <input type="radio" name="preguntaDiez" value="1">Baquet Ball<br>
    <input type="radio" name="preguntaDiez" value="0">Cantaba<br>


    <br>
    <input type="submit"  value="resultados" href="{{route('MisResultados.lis')}}>
</form>
@endsection

@section('js')

@endsection

