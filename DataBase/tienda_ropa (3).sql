-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-04-2021 a las 04:59:28
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_ropa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_Admin` int(100) NOT NULL,
  `nombre` varchar(1000) NOT NULL,
  `correo` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_Admin`, `nombre`, `correo`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Esteban', 'esteban@gmail.com', '$2y$10$/RdHfovU5ezjvOG9prLp/ezBZDwEGD0zZnWCd3UAaZOMCpgsReA7a', '2021-03-31 16:42:07', '2021-03-31 16:42:07'),
(2, 'Alexis', 'alex@gmail.com', '$2y$10$ZVSD0bNl6.hixB/3A0ygp.LzK69EiwmyGLiUT5VfEOyP3xl7kZQnu', '2021-04-01 13:22:21', '2021-04-01 13:22:21'),
(3, 'Diana', 'diana@gmail.com', '$2y$10$scJA4K8cYLnx432B6HVf1OT5axGwNMoaiw9F7mOrHItYL2/w4IUSK', '2021-04-02 11:57:47', '2021-04-02 11:57:47'),
(4, 'Isai', 'trokitas@gmail.com', '$2y$10$ntERpuvehDoLHZxEjITy7.1P6CO5k84DNK6EXBOUEMug0N3t.66gK', '2021-04-02 20:52:42', '2021-04-02 20:52:42'),
(5, 'esteban', 'prueba@gmail.com', '$2y$10$NNPYix4Fmax7nrAFKRIZCue7F5ro.7qr46IBL0.eXrJskcYPOuBFK', '2021-04-04 08:38:04', '2021-04-04 08:38:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id_carrito` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `marca` varchar(1000) NOT NULL,
  `cantidad` varchar(1000) NOT NULL,
  `descripcion` text NOT NULL,
  `total` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id_carrito`, `id_usuario`, `id_producto`, `producto`, `marca`, `cantidad`, `descripcion`, `total`, `created_at`, `updated_at`) VALUES
(7, 1, 3, 'balon Barca', 'nike', '2', 'Balon nike club barcelona abalado por la fifa', '500', '2021-04-04 06:54:08', '2021-04-04 06:54:08'),
(8, 1, 6, 'Balon NBA', 'NIKE', '5', 'Balon nike de basquetball', '360', '2021-04-04 06:54:08', '2021-04-04 06:54:08'),
(9, 1, 3, 'balon Barca', 'nike', '2', 'Balon nike club barcelona abalado por la fifa', '500', '2021-04-04 06:54:14', '2021-04-04 06:54:14'),
(10, 1, 6, 'Balon NBA', 'NIKE', '5', 'Balon nike de basquetball', '360', '2021-04-04 06:54:14', '2021-04-04 06:54:14'),
(11, 1, 8, 'Balon Champions', 'nike', '5', 'Balon nike champions 2021', '3750', '2021-04-04 06:56:38', '2021-04-04 06:56:38'),
(12, 1, 8, 'Balon Champions', 'nike', '5', 'Balon nike champions 2021', '3750', '2021-04-04 06:56:43', '2021-04-04 06:56:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_Usuario` int(11) NOT NULL,
  `producto` varchar(100) NOT NULL,
  `marca` varchar(1000) NOT NULL,
  `cantidad` varchar(1000) NOT NULL,
  `descripcion` text NOT NULL,
  `total` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id_compra`, `id_Usuario`, `producto`, `marca`, `cantidad`, `descripcion`, `total`, `created_at`, `updated_at`) VALUES
(1, 2, 'balon barca', 'nike', '2', 'Balon nike club barcelona abalado por la fifa', '150', '2021-04-04 07:53:56', '2021-04-04 07:53:56'),
(2, 1, 'Balon NBA', 'NIKE', '2', 'Balon nike de basquetball', '360', '2021-04-04 07:53:56', '2021-04-04 07:53:56'),
(3, 2, 'balon barca', 'nike', '2', 'Balon nike club barcelona abalado por la fifa', '150', '2021-04-04 07:54:04', '2021-04-04 07:54:04'),
(4, 1, 'Balon NBA', 'NIKE', '2', 'Balon nike de basquetball', '360', '2021-04-04 07:54:04', '2021-04-04 07:54:04'),
(5, 4, 'Balon Champions', 'NIKE', '5', 'Balon Champions', '3750', '2021-04-04 07:57:37', '2021-04-04 07:57:37'),
(6, 4, 'Playera Real Madrid', 'nike', '3', 'Playera Temporada 2021', '1500', '2021-04-04 07:57:37', '2021-04-04 07:57:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_prod` int(11) NOT NULL,
  `nombre` varchar(1000) NOT NULL,
  `marca` varchar(1000) NOT NULL,
  `imagen` varchar(10000) NOT NULL,
  `imagen2` varchar(1000) NOT NULL,
  `descripcion` text NOT NULL,
  `existencia` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_prod`, `nombre`, `marca`, `imagen`, `imagen2`, `descripcion`, `existencia`, `precio`, `created_at`, `updated_at`) VALUES
(3, 'balon Barca', 'nike', '/storage/imagenes/5SVRrIayAHhDOUjd9S4UW1CBLDFHYCgOtuKm0mxQ.jpg', '/storage/imagenes/QcHDzASD7p4DpbRJo8rvMv2yOZX9rucybRTFGD9r.jpg', 'Balon nike club barcelona abalado por la fifa', '10', '250', '2021-04-04 08:42:11', '2021-04-04 08:42:11'),
(6, 'Balon NBA', 'NIKE', '/storage/imagenes/i4wojXGGRPJX3cahvvTWNq1usyrQGdK9dvZtoiax.jpg', '/storage/imagenes/JXesSh3G5TOUBVOz6UhDnC1uGLLob1yasyLKELxH.jpg', 'Balon nike de basquetball', '25', '180', '2021-04-04 09:41:44', '2021-04-04 09:41:44'),
(7, 'Balon NBA', 'NIKE', '/storage/imagenes/BZOu6hRCxiqxrL3XkySvB3M8RETjByQIYLEAZcms.jpg', '/storage/imagenes/vE0NczFkKdLUqBRvwkdYWUfJhc2eT7lSbkZFkkYl.jpg', 'Balon nike de basquetball', '12', '450', '2021-04-04 10:38:10', '2021-04-04 10:38:10'),
(8, 'Balon Champions', 'nike', '/storage/imagenes/WIA595tIadqHKHKsNONe5xckA3f3aWC61FFRTr5x.jpg', '/storage/imagenes/J9qp3zkKVoigtNQy5T85bgiW0lvpe6nwN2Oh8iDZ.jpg', 'Balon nike champions 2021', '100', '750', '2021-04-04 10:40:36', '2021-04-04 10:40:36'),
(9, 'Playera Real Madrid', 'Adidas', '/storage/imagenes/q1CHyk3mhSVFKRzeK4RDLQnSp3gxeH6V24edn7SF.jpg', '/storage/imagenes/qujS7HVuHphMwi0Diw3D6rVkXU6Gw835RnOf29Yq.jpg', 'Playera temporada 2020', '150', '500', '2021-04-04 10:43:46', '2021-04-04 10:43:46'),
(10, 'Playera Barcelona', 'Nike', '/storage/imagenes/NBpmQwL3QJxzFYLbol3NE45Urz6OV6Sqqwle3GOa.png', '/storage/imagenes/DemwSpBzgRCbS4uklQmIEN9dqOCiT6PSr207QNqi.jpg', 'Playera Temporada 2021', '70', '450', '2021-04-04 10:46:06', '2021-04-04 10:46:06'),
(11, 'Playera Boleibol Mexico', 'Adidas', '/storage/imagenes/soQvyAgdA9MyznqjqUOuQ25e7kyBlhJXCnmbXXB5.jpg', '/storage/imagenes/hfWCUMD22bRjcfZgHS7kuJhZY6Y5HhPJcbMbzMkR.jpg', 'Playera Blanca para BolleiBall', '10', '70', '2021-04-04 10:48:15', '2021-04-04 10:48:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `nombre`, `correo`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Esteban', 'esteban@gmail.com', '$2y$10$NWL3p1TdpxrNMbQeZ82lh.buyXfNYH0vmG2QQjSps1r2TCXUf.0cm', '2021-03-30 19:39:55', '2021-03-30 19:39:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_Admin`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id_carrito`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_producto` (`id_producto`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_prod`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_Admin` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `id_carrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_prod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `carrito_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carrito_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_prod`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
